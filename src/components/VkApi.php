<?php

namespace semako\vkApi\components;

use semako\vkApi\enums\ApiRequestType;
use semako\vkApi\enums\ApiResponseFormat;
use semako\vkApi\enums\ApiVersion;
use semako\vkApi\interfaces\IVkApi;
use VK\VK;
use VK\VKException;
use yii\base\Component;

/**
 * Class VkApi
 * @package semako\vkApi\components
 */
class VkApi extends Component implements IVkApi
{
    /**
     * @var VK
     */
    private $vk;

    /**
     * @var int
     */
    public $appId;

    /**
     * @var string
     */
    public $apiSecret;

    /**
     * @var string
     */
    public $accessToken;

    /**
     * @var string
     */
    public $version = ApiVersion::VERSION;

    /**
     * Initializes the object.
     * This method is invoked at the end of the constructor after the object is initialized with the
     * given configuration.
     */
    public function init()
    {
        $this->vk = new VK($this->appId, $this->apiSecret, $this->accessToken);
        $this->vk->setApiVersion($this->version);

        parent::init();
    }

    /**
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @return string
     */
    public function getApiSecret()
    {
        return $this->apiSecret;
    }

    /**
     * @return int
     */
    public function getAppId()
    {
        return $this->appId;
    }

    /**
     * @param $accessToken
     * @return IVkApi
     */
    public function withToken($accessToken)
    {
        $this->setAccessToken($accessToken);
        return $this;
    }

    /**
     * Set special API version.
     * @param int $version
     * @return void
     */
    public function setApiVersion($version)
    {
        $this->vk->setApiVersion($version);
    }

    /**
     * Set Access Token.
     * @param string $accessToken
     * @throws VKException
     * @return void
     */
    public function setAccessToken($accessToken)
    {
        $this->vk->setAccessToken($accessToken);
    }

    /**
     * Returns base API url.
     * @param string $method
     * @return string
     */
    public function getApiUrl($method)
    {
        return $this->vk->getApiUrl($method);
    }

    /**
     * Returns authorization link with passed parameters.
     * @param string $apiSettings
     * @param string $callbackUrl
     * @param bool $testMode
     * @return string
     */
    public function getAuthorizeUrl($apiSettings = '', $callbackUrl = 'https://api.vk.com/blank.html', $testMode = false)
    {
        return $this->vk->getAuthorizeUrl($apiSettings, $callbackUrl, $testMode);
    }

    /**
     * Returns access token by code received on authorization link.
     * @param string $code
     * @param string $callbackUrl
     * @throws VKException
     * @return array
     */
    public function getAccessToken($code, $callbackUrl = 'https://api.vk.com/blank.html')
    {
        return $this->vk->getAccessToken($code, $callbackUrl);
    }

    /**
     * Return user authorization status.
     * @return bool
     */
    public function isAuth()
    {
        return $this->vk->isAuth();
    }

    /**
     * Check for validity access token.
     * @param string $accessToken
     * @return bool
     */
    public function checkAccessToken($accessToken = null)
    {
        return $this->vk->checkAccessToken($accessToken);
    }

    /**
     * Execute API method with parameters and return result.
     * @param string $method
     * @param array $parameters
     * @param string $format
     * @param string $requestMethod
     * @return array|object
     * @see ApiResponseFormat
     * @see ApiRequestType
     */
    public function api($method, $parameters = [], $format = ApiResponseFormat::FMT_ARRAY, $requestMethod = ApiRequestType::GET)
    {
        return $this->vk->api($method, $parameters, $format, $requestMethod);
    }
}
