<?php

namespace semako\vkApi\components;

use semako\vkApi\enums\AttachmentType;
use yii\base\InvalidValueException;

/**
 * Class AttachmentFactory
 * @package semako\vkApi\components
 */
abstract class AttachmentFactory
{
    /**
     * @var array
     */
    private static $map = [
        AttachmentType::ALBUM        => '\semako\vkApi\entity\Album',
        AttachmentType::APP          => '\semako\vkApi\entity\Application',
        AttachmentType::AUDIO        => '\semako\vkApi\entity\Audio',
        AttachmentType::DOC          => '\semako\vkApi\entity\Doc',
        AttachmentType::GRAFFITI     => '\semako\vkApi\entity\Graffiti',
        AttachmentType::LINK         => '\semako\vkApi\entity\Link',
        AttachmentType::MARKET       => '\semako\vkApi\entity\Market',
        AttachmentType::MARKET_ALBUM => '\semako\vkApi\entity\MarketAlbum',
        AttachmentType::NOTE         => '\semako\vkApi\entity\Note',
        AttachmentType::PAGE         => '\semako\vkApi\entity\Page',
        AttachmentType::PHOTO        => '\semako\vkApi\entity\Photo',
        AttachmentType::PHOTOS_LIST  => '\semako\vkApi\entity\PhotosList',
        AttachmentType::POLL         => '\semako\vkApi\entity\Poll',
        AttachmentType::POSTED_PHOTO => '\semako\vkApi\entity\PostedPhoto',
        AttachmentType::VIDEO        => '\semako\vkApi\entity\Video',
    ];

    /**
     * @param $type
     * @param array $data
     * @return object
     */
    public static function createAttachment($type, array $data)
    {
        if (!isset($type, self::$map)) {
            throw new InvalidValueException('($type = ' . $type . ') is invalid.');
        }

        $cls = self::$map[$type];
        return new $cls($data);
    }
}
