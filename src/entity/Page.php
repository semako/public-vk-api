<?php

namespace semako\vkApi\entity;

use semako\vkApi\interfaces\IPage;
use semako\yii2Common\traits\ClassName;
use semako\yii2Common\traits\ToArray;

/**
 * Class Page
 * @package semako\vkApi\entity
 * @link https://new.vk.com/dev/page
 */
class Page implements IPage
{
    use ClassName, ToArray;
}
