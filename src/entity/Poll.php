<?php

namespace semako\vkApi\entity;

use semako\vkApi\interfaces\IAnswer;
use semako\vkApi\interfaces\IPoll;
use semako\vkApi\traits\WithDate;
use semako\vkApi\traits\WithId;
use semako\vkApi\traits\WithOwnerId;
use semako\yii2Common\traits\ClassName;
use semako\yii2Common\traits\ToArray;
use yii\web\NotAcceptableHttpException;

/**
 * Class Poll
 * @package semako\vkApi\entity
 * @link https://new.vk.com/dev/attachments_w?f=10.%20%D0%9E%D0%BF%D1%80%D0%BE%D1%81%20(type%20%3D%20poll)
 */
class Poll implements IPoll
{
    use ClassName, ToArray, WithId, WithOwnerId, WithDate;

    /**
     * @var string
     */
    private $question;

    /**
     * @var int
     */
    private $countVotes;

    /**
     * @var int|null
     */
    private $userChoice;

    /**
     * @var IAnswer[]
     */
    private $answers;

    /**
     * @var bool
     */
    private $anonymous;

    /**
     * Post constructor.
     * @param array $data
     * @throws NotAcceptableHttpException
     */
    public function __construct(array $data)
    {
        if (!isset($data['id'])) {
            throw new NotAcceptableHttpException();
        }

        $this->id   = (int) $data['id'];
        $this->ownerId = (int) $data['owner_id'];
        $this->date = (int) $data['created'];
        $this->question = isset($data['question']) && !empty($data['question']) ? (string) $data['question'] : null;
        $this->countVotes = (int) $data['votes'];
        $this->userChoice = isset($data['answer_id']) ? (int) $data['answer_id'] : null;
        $this->anonymous = isset($data['anonymous']) ? (bool) $data['anonymous'] : false;

        if (isset($data['answers']) && sizeof($data['answers'])) {
            $this->processAnswers($data['answers']);
        }
    }

    /**
     * @param array $answers
     */
    private function processAnswers(array $answers)
    {
        foreach ($answers as $item) {
            $this->answers[] = new Answer($item);
        }
    }

    /**
     * @return IAnswer[]
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * @return int|null
     */
    public function getUserChoice()
    {
        return $this->userChoice;
    }

    /**
     * @return int
     */
    public function getCountVotes()
    {
        return $this->countVotes;
    }

    /**
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @return bool
     */
    public function isAnonymous()
    {
        return $this->anonymous;
    }
}
