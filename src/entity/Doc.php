<?php

namespace semako\vkApi\entity;

use semako\vkApi\interfaces\IDoc;
use semako\vkApi\traits\WithDate;
use semako\vkApi\traits\WithId;
use semako\vkApi\traits\WithPhotos;
use semako\vkApi\traits\WithTitle;
use semako\vkApi\traits\WithUrl;
use semako\yii2Common\traits\ClassName;
use semako\yii2Common\traits\ToArray;

/**
 * Class Doc
 * @package semako\vkApi\entity
 * @link https://new.vk.com/dev/doc
 */
class Doc implements IDoc
{
    use ClassName, ToArray, WithId, WithTitle, WithUrl, WithDate, WithPhotos;

    /**
     * @var int
     */
    private $fileSize;

    /**
     * @var string
     */
    private $ext;

    /**
     * @var int
     * @see DocType
     */
    private $type;

    /**
     * @return int
     */
    public function getFileSize()
    {
        return $this->fileSize;
    }

    /**
     * @return string
     */
    public function getExt()
    {
        return $this->ext;
    }

    /**
     * @return int
     * @see DocType
     */
    public function getType()
    {
        return $this->type;
    }
}
