<?php

namespace semako\vkApi\entity;

use semako\vkApi\interfaces\INote;
use semako\vkApi\traits\WithDate;
use semako\vkApi\traits\WithId;
use semako\vkApi\traits\WithOwnerId;
use semako\vkApi\traits\WithText;
use semako\vkApi\traits\WithTitle;
use semako\yii2Common\traits\ClassName;
use semako\yii2Common\traits\ToArray;

/**
 * Class Note
 * @package semako\vkApi\entity
 * @link https://new.vk.com/dev/note
 */
class Note implements INote
{
    use ClassName, ToArray, WithId, WithOwnerId, WithTitle, WithText, WithDate;

    /**
     * @var int
     */
    private $countComments;

    /**
     * @var int
     */
    private $countReadComments;

    /**
     * @var string
     */
    private $viewUrl;

    /**
     * @return int
     */
    public function getCountComments()
    {
        return $this->countComments;
    }

    /**
     * @return int
     */
    public function getCountReadComments()
    {
        return $this->countReadComments;
    }

    /**
     * @return string
     */
    public function getViewUrl()
    {
        return $this->viewUrl;
    }
}
