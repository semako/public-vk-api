<?php

namespace semako\vkApi\entity;

use semako\vkApi\interfaces\ICurrency;
use semako\vkApi\interfaces\IPrice;
use semako\vkApi\traits\WithText;
use semako\yii2Common\traits\ClassName;
use semako\yii2Common\traits\ToArray;

/**
 * Class Poll
 * @package semako\vkApi\entity
 * @link https://new.vk.com/dev/price
 */
class Price implements IPrice
{
    use ClassName, ToArray, WithText;

    /**
     * @var ICurrency
     */
    private $currency;

    /**
     * @var int
     */
    private $amount;

    /**
     * @return ICurrency
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }
}
