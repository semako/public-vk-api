<?php

namespace semako\vkApi\entity;

use semako\vkApi\interfaces\IPhoto;
use semako\vkApi\interfaces\IPhotosList;
use semako\yii2Common\traits\ClassName;
use semako\yii2Common\traits\ToArray;

/**
 * Class PhotosList
 * @package semako\vkApi\entity
 * @link https://new.vk.com/dev/attachments_w?f=13.%20%D0%A1%D0%BF%D0%B8%D1%81%D0%BE%D0%BA%20%D1%84%D0%BE%D1%82%D0%BE%D0%B3%D1%80%D0%B0%D1%84%D0%B8%D0%B9%20(type%3Dphotos_list)
 */
class PhotosList implements IPhotosList
{
    use ClassName, ToArray;

    /**
     * @var IPhoto[]
     */
    private $photos = [];

    /**
     * @return IPhoto[]
     */
    public function getPhotos()
    {
        return $this->photos;
    }
}
