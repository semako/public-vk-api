<?php

namespace semako\vkApi\entity;

use semako\vkApi\interfaces\IVideo;
use semako\vkApi\traits\WithDate;
use semako\vkApi\traits\WithDescription;
use semako\vkApi\traits\WithId;
use semako\vkApi\traits\WithOwnerId;
use semako\vkApi\traits\WithPhotos;
use semako\vkApi\traits\WithTitle;
use semako\yii2Common\traits\ClassName;
use semako\yii2Common\traits\ToArray;
use yii\web\NotAcceptableHttpException;

/**
 * Class Video
 * @package semako\vkApi\entity
 */
class Video implements IVideo
{
    use ClassName, ToArray, WithId, WithOwnerId, WithTitle, WithDescription, WithPhotos, WithDate;

    /**
     * @var int
     */
    private $duration;

    /**
     * @var int|null
     */
    private $dateAdded;

    /**
     * @var int
     */
    private $countViews;

    /**
     * @var int
     */
    private $countComments;

    /**
     * @var string
     */
    private $playerUrl;

    /**
     * @var string|null
     */
    private $accessKey;

    /**
     * @var bool
     */
    private $processing;

    /**
     * @var bool
     */
    private $live;

    /**
     * Video constructor.
     * @param array $data
     * @throws NotAcceptableHttpException
     */
    public function __construct(array $data)
    {
        if (!isset($data['id'])) {
            throw new NotAcceptableHttpException();
        }

        $this->id   = (int) $data['id'];
        $this->ownerId = (int) $data['owner_id'];
        $this->title = isset($data['title']) && !empty($data['title']) ? (string) $data['title'] : null;
        $this->description = isset($data['description']) && !empty($data['description']) ? (string) $data['description'] : null;
        $this->date = (int) $data['date'];
        $this->duration = isset($data['duration']) && !empty($data['duration']) ? (int) $data['duration'] : null;
        $this->dateAdded = isset($data['adding_date']) && !empty($data['adding_date']) ? (int) $data['adding_date'] : null;
        $this->countViews = (int) $data['views'];
        $this->countComments = (int) $data['comments'];
        $this->playerUrl = isset($data['player']) && !empty($data['player']) ? (string) $data['player'] : null;
        $this->accessKey = isset($data['access_key']) && !empty($data['access_key']) ? (string) $data['access_key'] : null;
        $this->processing = isset($data['processing']) ? (bool) $data['processing'] : false;
        $this->live = isset($data['live']) ? (bool) $data['live'] : false;

        $this->parsePhotos($data);
    }

    /**
     * @return int
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @return int|null
     */
    public function getDateAdded()
    {
        return $this->dateAdded;
    }

    /**
     * @return int
     */
    public function getCountViews()
    {
        return $this->countViews;
    }

    /**
     * @return int
     */
    public function getCountComments()
    {
        return $this->countComments;
    }

    /**
     * @return string
     */
    public function getPlayerUrl()
    {
        return $this->playerUrl;
    }

    /**
     * @return null|string
     */
    public function getAccessKey()
    {
        return $this->accessKey;
    }

    /**
     * @return boolean
     */
    public function isProcessing()
    {
        return $this->processing;
    }

    /**
     * @return boolean
     */
    public function isLive()
    {
        return $this->live;
    }
}
