<?php

namespace semako\vkApi\entity;

use semako\vkApi\interfaces\IPhoto;
use semako\vkApi\traits\WithDate;
use semako\vkApi\traits\WithId;
use semako\vkApi\traits\WithOwnerId;
use semako\vkApi\traits\WithPhotos;
use semako\vkApi\traits\WithText;
use semako\yii2Common\traits\ClassName;
use semako\yii2Common\traits\ToArray;
use yii\web\NotAcceptableHttpException;

/**
 * Class Photo
 * @package semako\vkApi\entity
 * @link https://new.vk.com/dev/photo
 */
class Photo implements IPhoto
{
    use ClassName, ToArray, WithId, WithOwnerId, WithText, WithDate, WithPhotos;

    /**
     * @var int
     */
    private $albumId;

    /**
     * @var int|null
     */
    private $userId;

    /**
     * @var int|null
     */
    private $originalWidth;

    /**
     * @var int|null
     */
    private $originalHeight;

    /**
     * Post constructor.
     * @param array $data
     * @throws NotAcceptableHttpException
     */
    public function __construct(array $data)
    {
        if (!isset($data['id'])) {
            throw new NotAcceptableHttpException();
        }

        $this->id   = (int) $data['id'];
        $this->ownerId = (int) $data['owner_id'];
        $this->albumId = (int) $data['album_id'];
        $this->userId = isset($data['user_id']) && !empty($data['user_id']) ? (int) $data['user_id'] : null;
        $this->text = isset($data['text']) && !empty($data['text']) ? (string) $data['text'] : null;
        $this->date = (int) $data['date'];
        $this->originalWidth = isset($data['width']) && !empty($data['width']) ? (int) $data['width'] : null;
        $this->originalHeight = isset($data['height']) && !empty($data['height']) ? (int) $data['height'] : null;

        $this->parsePhotos($data);
    }

    /**
     * @return int
     */
    public function getAlbumId()
    {
        return $this->albumId;
    }

    /**
     * @return int|null
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return int|null
     */
    public function getOriginalWidth()
    {
        return $this->originalWidth;
    }

    /**
     * @return int|null
     */
    public function getOriginalHeight()
    {
        return $this->originalHeight;
    }
}
