<?php

namespace semako\vkApi\entity;

use semako\vkApi\interfaces\IPostedPhoto;
use semako\vkApi\traits\WithId;
use semako\vkApi\traits\WithOwnerId;
use semako\vkApi\traits\WithPhotos;
use semako\yii2Common\traits\ClassName;
use semako\yii2Common\traits\ToArray;

/**
 * Class PostedPhoto
 * @package semako\vkApi\entity
 * @link https://new.vk.com/dev/attachments_w?f=2.%20%D0%A4%D0%BE%D1%82%D0%BE%D0%B3%D1%80%D0%B0%D1%84%D0%B8%D1%8F%2C%20%D0%B7%D0%B0%D0%B3%D1%80%D1%83%D0%B6%D0%B5%D0%BD%D0%BD%D0%B0%D1%8F%20%D0%BD%D0%B0%D0%BF%D1%80%D1%8F%D0%BC%D1%83%D1%8E%20(type%20%3D%20posted_photo)
 */
class PostedPhoto implements IPostedPhoto
{
    use ClassName, ToArray, WithId, WithOwnerId, WithPhotos;
}
