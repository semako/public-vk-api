<?php

namespace semako\vkApi\entity;

use semako\vkApi\interfaces\ICurrency;
use semako\vkApi\traits\WithId;
use semako\vkApi\traits\WithName;
use semako\yii2Common\traits\ClassName;
use semako\yii2Common\traits\ToArray;

/**
 * Class Currency
 * @package semako\vkApi\entity
 * @link https://new.vk.com/dev/price
 */
class Currency implements ICurrency
{
    use ClassName, ToArray, WithId, WithName;
}
