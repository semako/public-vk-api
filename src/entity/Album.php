<?php

namespace semako\vkApi\entity;

use semako\vkApi\interfaces\IAlbum;
use semako\vkApi\traits\WithDescription;
use semako\vkApi\traits\WithId;
use semako\vkApi\traits\WithOwnerId;
use semako\vkApi\traits\WithThumb;
use semako\vkApi\traits\WithTitle;
use semako\yii2Common\traits\ClassName;
use semako\yii2Common\traits\ToArray;

/**
 * Class Album
 * @package semako\vkApi\entity
 * @link https://new.vk.com/dev/attachments_w?f=12.%20%D0%90%D0%BB%D1%8C%D0%B1%D0%BE%D0%BC%20%D1%81%20%D1%84%D0%BE%D1%82%D0%BE%D0%B3%D1%80%D0%B0%D1%84%D0%B8%D1%8F%D0%BC%D0%B8%20(type%3Dalbum)
 */
class Album implements IAlbum
{
    use ClassName, ToArray, WithId, WithOwnerId, WithThumb, WithTitle, WithDescription;

    /**
     * @var int
     */
    private $dateCreated;

    /**
     * @var int|null
     */
    private $dateUpdated;

    /**
     * @var int
     */
    private $countPhotos = 0;

    /**
     * @return int
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @return int|null
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @return int
     */
    public function getCountPhotos()
    {
        return $this->countPhotos;
    }
}
