<?php

namespace semako\vkApi\entity;

use semako\vkApi\interfaces\IAudio;
use semako\vkApi\traits\WithDate;
use semako\vkApi\traits\WithId;
use semako\vkApi\traits\WithOwnerId;
use semako\vkApi\traits\WithTitle;
use semako\vkApi\traits\WithUrl;
use semako\yii2Common\traits\ClassName;
use semako\yii2Common\traits\ToArray;
use yii\web\NotAcceptableHttpException;

/**
 * Class Audio
 * @package semako\vkApi\entity
 * @link https://new.vk.com/dev/audio_object
 */
class Audio implements IAudio
{
    use ClassName, ToArray, WithId, WithOwnerId, WithTitle, WithDate, WithUrl;

    /**
     * @var string
     */
    private $artist;

    /**
     * @var int
     */
    private $duration;

    /**
     * @var int|null
     */
    private $lyricsId;

    /**
     * @var int|null
     */
    private $albumId;

    /**
     * @var int|null
     */
    private $genreId;

    /**
     * @var bool
     */
    private $noSearch;

    /**
     * Video constructor.
     * @param array $data
     * @throws NotAcceptableHttpException
     */
    public function __construct(array $data)
    {
        if (!isset($data['id'])) {
            throw new NotAcceptableHttpException();
        }

        $this->id   = (int) $data['id'];
        $this->ownerId = (int) $data['owner_id'];
        $this->title = isset($data['title']) && !empty($data['title']) ? (string) $data['title'] : null;
        $this->date = (int) $data['date'];
        $this->duration = isset($data['duration']) && !empty($data['duration']) ? (int) $data['duration'] : null;
        $this->url = (string) $data['url'];
        $this->artist = isset($data['artist']) && !empty($data['artist']) ? (string) $data['artist'] : null;
        $this->lyricsId = isset($data['lyrics_id']) && !empty($data['lyrics_id']) ? (int) $data['lyrics_id'] : null;
        $this->albumId = isset($data['album_id']) && !empty($data['album_id']) ? (int) $data['album_id'] : null;
        $this->genreId = isset($data['genre_id']) && !empty($data['genre_id']) ? (int) $data['genre_id'] : null;
        $this->noSearch = isset($data['no_search']) ? (bool) $data['no_search'] : false;
    }

    /**
     * @return string
     */
    public function getArtist()
    {
        return $this->artist;
    }

    /**
     * @return int
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @return int|null
     */
    public function getLyricsId()
    {
        return $this->lyricsId;
    }

    /**
     * @return int|null
     */
    public function getAlbumId()
    {
        return $this->albumId;
    }

    /**
     * @return int|null
     */
    public function getGenreId()
    {
        return $this->genreId;
    }

    /**
     * @return bool
     */
    public function isNoSearch()
    {
        return $this->noSearch;
    }
}
