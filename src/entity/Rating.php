<?php

namespace semako\vkApi\entity;

use semako\vkApi\interfaces\IRating;
use semako\yii2Common\traits\ClassName;
use semako\yii2Common\traits\ToArray;

/**
 * Class Rating
 * @package semako\vkApi\entity
 * @link https://new.vk.com/dev/link_rating
 */
class Rating implements IRating
{
    use ClassName, ToArray;

    /**
     * @var int
     */
    private $stars;

    /**
     * @var int
     */
    private $countReviews;

    /**
     * @return int
     */
    public function getStars()
    {
        return $this->stars;
    }

    /**
     * @return int
     */
    public function getCountReviews()
    {
        return $this->countReviews;
    }
}
