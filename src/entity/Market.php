<?php

namespace semako\vkApi\entity;

use semako\vkApi\interfaces\IMarket;
use semako\yii2Common\traits\ClassName;
use semako\yii2Common\traits\ToArray;

/**
 * Class Market
 * @package semako\vkApi\entity
 * @link https://new.vk.com/dev/item
 */
class Market implements IMarket
{
    use ClassName, ToArray;
}
