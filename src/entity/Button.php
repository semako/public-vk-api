<?php

namespace semako\vkApi\entity;

use semako\vkApi\interfaces\IButton;
use semako\vkApi\traits\WithTitle;
use semako\vkApi\traits\WithUrl;
use semako\yii2Common\traits\ClassName;
use semako\yii2Common\traits\ToArray;

/**
 * Class Button
 * @package semako\vkApi\entity
 * @link https://new.vk.com/dev/link_button
 */
class Button implements IButton
{
    use ClassName, ToArray, WithTitle, WithUrl;
}
