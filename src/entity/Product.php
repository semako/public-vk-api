<?php

namespace semako\vkApi\entity;

use semako\vkApi\interfaces\IPrice;
use semako\vkApi\interfaces\IProduct;
use semako\yii2Common\traits\ClassName;
use semako\yii2Common\traits\ToArray;

/**
 * Class Product
 * @package semako\vkApi\entity
 * @link https://new.vk.com/dev/link_product
 */
class Product implements IProduct
{
    use ClassName, ToArray;

    /**
     * @var IPrice
     */
    private $price;

    /**
     * @return IPrice
     */
    public function getPrice()
    {
        return $this->price;
    }
}
