<?php

namespace semako\vkApi\entity;

use semako\vkApi\components\AttachmentFactory;
use semako\vkApi\enums\AttachmentType;
use semako\vkApi\enums\PostType;
use semako\vkApi\interfaces\IPost;
use semako\vkApi\traits\WithDate;
use semako\vkApi\traits\WithId;
use semako\vkApi\traits\WithOwnerId;
use semako\vkApi\traits\WithText;
use semako\yii2Common\traits\ClassName;
use semako\yii2Common\traits\ToArray;
use yii\web\NotAcceptableHttpException;

/**
 * Class Post
 * @package semako\vkApi\entity
 * @link https://new.vk.com/dev/post
 */
class Post implements IPost
{
    use ClassName, ToArray, WithId, WithDate, WithText, WithOwnerId;

    /**
     * @var int
     */
    private $fromId;

    /**
     * @var int|null
     */
    private $replyOwnerId;

    /**
     * @var int|null
     */
    private $replyPostId;

    /**
     * @var int
     */
    private $countComments = 0;

    /**
     * @var int
     */
    private $countLikes = 0;

    /**
     * @var int
     */
    private $countReposts = 0;

    /**
     * @var int
     */
    private $countPhotos = 0;

    /**
     * @var int
     */
    private $countVideos = 0;

    /**
     * @var string
     * @see PostType
     */
    private $postType;

    /**
     * @var object[]
     */
    private $attachments;

    /**
     * @var int|null
     */
    private $signerId;

    /**
     * @var bool
     */
    private $canPin = false;

    /**
     * @var bool
     */
    private $canDelete = false;

    /**
     * @var bool
     */
    private $canEdit = false;

    /**
     * @var bool
     */
    private $pinned = false;

    /**
     * Post constructor.
     * @param array $data
     * @throws NotAcceptableHttpException
     */
    public function __construct(array $data)
    {
        if (!isset($data['id'])) {
            throw new NotAcceptableHttpException();
        }

        $this->id   = (int) $data['id'];
        $this->ownerId = (int) $data['owner_id'];
        $this->date = (int) $data['date'];
        $this->postType = $data['post_type'];
        $this->text = isset($data['text']) && !empty($data['text']) ? (string) $data['text'] : null;
        $this->countComments = isset($data['comments']['count']) ? (int) $data['comments']['count'] : 0;
        $this->countLikes = isset($data['likes']['count']) ? (int) $data['likes']['count'] : 0;
        $this->countReposts = isset($data['reposts']['count']) ? (int) $data['reposts']['count'] : 0;
        $this->canPin = isset($data['can_pin']) ? (bool) $data['can_pin'] : false;
        $this->canDelete = isset($data['can_delete']) ? (bool) $data['can_delete'] : false;
        $this->canEdit = isset($data['can_edit']) ? (bool) $data['can_edit'] : false;
        $this->pinned = isset($data['is_pinned']) ? (bool) $data['is_pinned'] : false;
        $this->signerId = isset($data['signer_id']) ? (int) $data['signer_id'] : null;
        $this->replyOwnerId = isset($data['reply_owner_id']) ? (int) $data['reply_owner_id'] : null;
        $this->replyPostId = isset($data['reply_post_id']) ? (int) $data['reply_post_id'] : null;
        $this->fromId = isset($data['from_id']) ? (int) $data['from_id'] : null;

        if (isset($data['attachments']) && sizeof($data['attachments'])) {
            $this->processAttachments($data['attachments']);
        }
    }

    /**
     * @param array $attachments
     */
    private function processAttachments(array $attachments)
    {
        foreach ($attachments as $item) {
            $this->attachments[] = AttachmentFactory::createAttachment($item['type'], $item[$item['type']]);

            // Update counters
            switch ($item['type']) {
                case AttachmentType::PHOTO:
                    $this->countPhotos++;
                    break;

                case AttachmentType::VIDEO:
                    $this->countVideos++;
                    break;

                default:
                    break;
            }
        }
    }

    /**
     * @return boolean
     */
    public function isPinned()
    {
        return $this->pinned;
    }

    /**
     * @return int
     */
    public function getFromId()
    {
        return $this->fromId;
    }

    /**
     * @return int|null
     */
    public function getReplyOwnerId()
    {
        return $this->replyOwnerId;
    }

    /**
     * @return int|null
     */
    public function getReplyPostId()
    {
        return $this->replyPostId;
    }

    /**
     * @return int
     */
    public function getCountComments()
    {
        return $this->countComments;
    }

    /**
     * @return int
     */
    public function getCountLikes()
    {
        return $this->countLikes;
    }

    /**
     * @return int
     */
    public function getCountReposts()
    {
        return $this->countReposts;
    }

    /**
     * @return int
     */
    public function getCountVideos()
    {
        return $this->countVideos;
    }

    /**
     * @return int
     */
    public function getCountPhotos()
    {
        return $this->countPhotos;
    }

    /**
     * @return string
     */
    public function getPostType()
    {
        return $this->postType;
    }

    /**
     * @return \object[]
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * @return int|null
     */
    public function getSignerId()
    {
        return $this->signerId;
    }

    /**
     * @return boolean
     */
    public function isCanPin()
    {
        return $this->canPin;
    }

    /**
     * @return boolean
     */
    public function isCanDelete()
    {
        return $this->canDelete;
    }

    /**
     * @return boolean
     */
    public function isCanEdit()
    {
        return $this->canEdit;
    }
}
