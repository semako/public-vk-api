<?php

namespace semako\vkApi\entity;

use semako\vkApi\interfaces\IAnswer;
use semako\vkApi\traits\WithId;
use semako\vkApi\traits\WithText;
use semako\yii2Common\traits\ClassName;
use semako\yii2Common\traits\ToArray;
use yii\web\NotAcceptableHttpException;

/**
 * Class Answer
 * @package semako\vkApi\entity
 * @link https://new.vk.com/dev/attachments_w?f=10.%20%D0%9E%D0%BF%D1%80%D0%BE%D1%81%20(type%20%3D%20poll)
 */
class Answer implements IAnswer
{
    use ClassName, ToArray, WithId, WithText;

    /**
     * @var int
     */
    private $countVotes = 0;

    /**
     * @var int
     */
    private $rating = 0;

    /**
     * Answer constructor.
     * @param array $data
     * @throws NotAcceptableHttpException
     */
    public function __construct(array $data)
    {
        if (!isset($data['id'])) {
            throw new NotAcceptableHttpException();
        }

        $this->id   = (int) $data['id'];
        $this->text = isset($data['text']) && !empty($data['text']) ? (string) $data['text'] : null;
        $this->countVotes = isset($data['votes']) ? (int) $data['votes'] : 0;
        $this->rating = isset($data['rate']) ? (float) $data['rate'] : 0.0;
    }

    /**
     * @return int
     */
    public function getCountVotes()
    {
        return $this->countVotes;
    }

    /**
     * @return int
     */
    public function getRating()
    {
        return $this->rating;
    }
}
