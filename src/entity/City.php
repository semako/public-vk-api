<?php

namespace semako\vkApi\entity;

use semako\vkApi\interfaces\ICity;
use semako\vkApi\traits\WithId;
use semako\vkApi\traits\WithTitle;
use semako\yii2Common\traits\ClassName;
use semako\yii2Common\traits\ToArray;
use yii\web\NotAcceptableHttpException;

/**
 * Class City
 * @package semako\vkApi\entity
 */
class City implements ICity
{
    use ClassName, ToArray, WithId, WithTitle;

    /**
     * Country constructor.
     * @param array $data
     * @throws NotAcceptableHttpException
     */
    public function __construct(array $data)
    {
        if (!isset($data['id'])) {
            throw new NotAcceptableHttpException();
        }

        $this->id = (int) $data['id'];
        $this->title = $data['title'];
    }
}
