<?php

namespace semako\vkApi\entity;

use semako\vkApi\interfaces\ILink;
use semako\vkApi\traits\WithApplication;
use semako\vkApi\traits\WithButton;
use semako\vkApi\traits\WithDescription;
use semako\vkApi\traits\WithPhoto;
use semako\vkApi\traits\WithProduct;
use semako\vkApi\traits\WithRating;
use semako\vkApi\traits\WithTitle;
use semako\vkApi\traits\WithUrl;
use semako\yii2Common\traits\ClassName;
use semako\yii2Common\traits\ToArray;
use yii\web\NotAcceptableHttpException;

/**
 * Class Link
 * @package semako\vkApi\entity
 * @link https://new.vk.com/dev/attachments_w?f=7.%20%D0%A1%D1%81%D1%8B%D0%BB%D0%BA%D0%B0%20(type%20%3D%20link)
 */
class Link implements ILink
{
    use ClassName, ToArray, WithUrl, WithTitle, WithDescription, WithPhoto, WithProduct, WithRating, WithApplication, WithButton;

    /**
     * @var string|null
     */
    private $caption;

    /**
     * @var bool
     */
    private $external;

    /**
     * @var string|null
     */
    private $previewPage;

    /**
     * @var string|null
     */
    private $previewUrl;

    /**
     * Link constructor.
     * @param array $data
     * @throws NotAcceptableHttpException
     */
    public function __construct(array $data)
    {
        if (!isset($data['url'])) {
            throw new NotAcceptableHttpException();
        }

        $this->url = (string) $data['url'];
        $this->title = isset($data['title']) && !empty($data['title']) ? (string) $data['title'] : null;
        $this->description = isset($data['description']) && !empty($data['description']) ? (string) $data['description'] : null;
        $this->caption = isset($data['caption']) && !empty($data['caption']) ? (string) $data['caption'] : null;
        $this->previewPage = isset($data['preview_page']) && !empty($data['preview_page']) ? (string) $data['preview_page'] : null;
        $this->previewUrl = isset($data['preview_url']) && !empty($data['preview_url']) ? (string) $data['preview_url'] : null;
        $this->photo = isset($data['photo']) && !empty($data['photo']) ? new Photo($data['photo']) : null;
        $this->product = isset($data['product']) && !empty($data['product']) ? new Product($data['product']) : null;
        $this->rating = isset($data['rating']) && !empty($data['rating']) ? new Rating($data['rating']) : null;
        $this->application = isset($data['application']) && !empty($data['application']) ? new Application($data['application']) : null;
        $this->button = isset($data['button']) && !empty($data['button']) ? new Button($data['button']) : null;
        $this->external = isset($data['is_external']) ? (bool) $data['is_external'] : false;
    }

    /**
     * @return string|null
     */
    public function getCaption()
    {
        return $this->caption;
    }

    /**
     * @return bool
     */
    public function isExternal()
    {
        return $this->external;
    }

    /**
     * @return string|null
     */
    public function getPreviewPage()
    {
        return $this->previewPage;
    }

    /**
     * @return string|null
     */
    public function getPreviewUrl()
    {
        return $this->previewUrl;
    }
}
