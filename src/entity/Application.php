<?php

namespace semako\vkApi\entity;

use semako\vkApi\interfaces\IApplication;
use semako\vkApi\traits\WithId;
use semako\vkApi\traits\WithName;
use semako\yii2Common\traits\ClassName;
use semako\yii2Common\traits\ToArray;

/**
 * Class Application
 * @package semako\vkApi\entity
 * @link https://new.vk.com/dev/attachments_w?f=9.%20%D0%9A%D0%BE%D0%BD%D1%82%D0%B5%D0%BD%D1%82%20%D0%BF%D1%80%D0%B8%D0%BB%D0%BE%D0%B6%D0%B5%D0%BD%D0%B8%D1%8F%20(type%20%3D%20app)
 */
class Application implements IApplication
{
    use ClassName, ToArray, WithId, WithName;

    /**
     * @var string
     */
    private $thumbPhotoUrl;

    /**
     * @var string
     */
    private $photoUrl;

    /**
     * @return string
     */
    public function getThumbPhotoUrl()
    {
        return $this->thumbPhotoUrl;
    }

    /**
     * @return string
     */
    public function getPhotoUrl()
    {
        return $this->photoUrl;
    }
}
