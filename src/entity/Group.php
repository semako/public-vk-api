<?php

namespace semako\vkApi\entity;

use semako\vkApi\interfaces\IGroup;
use semako\vkApi\traits\WithId;
use semako\vkApi\traits\WithName;
use semako\vkApi\traits\WithPhotos;
use semako\vkApi\traits\WithSlug;
use semako\yii2Common\traits\ClassName;
use semako\yii2Common\traits\ToArray;
use yii\web\NotAcceptableHttpException;

/**
 * Class Group
 * @package semako\vkApi\entity
 * @link https://new.vk.com/dev/group
 */
class Group implements IGroup
{
    use ClassName, ToArray, WithId, WithName, WithSlug, WithPhotos;

    /**
     * @var boolean
     */
    private $closed = false;

    /**
     * @var boolean
     */
    private $admin = false;

    /**
     * @var boolean
     */
    private $member = false;

    /**
     * @var boolean
     */
    private $canPost = false;

    /**
     * @var boolean
     */
    private $canSeeAllPosts = false;

    /**
     * @var boolean
     */
    private $verified = false;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string|null
     */
    private $site;

    /**
     * @var Country|null
     */
    private $country;

    /**
     * @var City|null
     */
    private $city;

    /**
     * @var string|null
     */
    private $description;

    /**
     * @var int
     */
    private $members;

    /**
     * @var string|null
     */
    private $status;

    /**
     * @var int
     */
    private $countPhotos = 0;

    /**
     * @var int
     */
    private $countAlbums = 0;

    /**
     * @var int
     */
    private $countTopics = 0;

    /**
     * @var int
     */
    private $countVideos = 0;

    /**
     * @var int
     */
    private $countDocs = 0;

    /**
     * @var int
     */
    private $countRecords = 0;

    /**
     * Group constructor.
     * @param array $data
     * @throws NotAcceptableHttpException
     */
    public function __construct(array $data)
    {
        if (!isset($data['id'])) {
            throw new NotAcceptableHttpException();
        }

        $this->id   = (int) $data['id'];
        $this->name = $data['name'];
        $this->slug = $data['screen_name'];
        $this->closed = isset($data['is_closed']) ? (bool) $data['is_closed'] : false;
        $this->admin = isset($data['is_admin']) ? (bool) $data['is_admin'] : false;
        $this->member = isset($data['is_member']) ? (bool) $data['is_member'] : false;
        $this->canPost = isset($data['can_post']) ? (bool) $data['can_post'] : false;
        $this->canSeeAllPosts = isset($data['can_see_all_posts']) ? (bool) $data['can_see_all_posts'] : false;
        $this->verified = isset($data['verified']) ? (bool) $data['verified'] : false;
        $this->status = !empty($data['status']) ? $data['status'] : null;
        $this->description = !empty($data['description']) ? $data['description'] : null;
        $this->type = $data['type'];
        $this->members = isset($data['members_count']) ? (int) $data['members_count'] : 0;
        $this->country = isset($data['country']) ? new Country($data['country']) : null;
        $this->city = isset($data['city']) ? new City($data['city']) : null;
        $this->countAlbums = isset($data['counters']['albums']) ? (int) $data['counters']['albums'] : 0;
        $this->countPhotos = isset($data['counters']['photos']) ? (int) $data['counters']['photos'] : 0;
        $this->countTopics = isset($data['counters']['topics']) ? (int) $data['counters']['topics'] : 0;
        $this->countVideos = isset($data['counters']['videos']) ? (int) $data['counters']['videos'] : 0;
        $this->countDocs = isset($data['counters']['docs']) ? (int) $data['counters']['docs'] : 0;
        $this->countRecords = isset($data['counters']['records']) ? (int) $data['counters']['records'] : 0;
        
        $this->parsePhotos($data);
    }

    /**
     * @return boolean
     */
    public function isClosed()
    {
        return $this->closed;
    }

    /**
     * @return boolean
     */
    public function isAdmin()
    {
        return $this->admin;
    }

    /**
     * @return boolean
     */
    public function isMember()
    {
        return $this->member;
    }

    /**
     * @return boolean
     */
    public function isCanPost()
    {
        return $this->canPost;
    }

    /**
     * @return boolean
     */
    public function isCanSeeAllPosts()
    {
        return $this->canSeeAllPosts;
    }

    /**
     * @return boolean
     */
    public function isVerified()
    {
        return $this->verified;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return null|string
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * @return Country|null
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @return City|null
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return null|string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return int
     */
    public function getMembers()
    {
        return $this->members;
    }

    /**
     * @return null|string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return int
     */
    public function getCountPhotos()
    {
        return $this->countPhotos;
    }

    /**
     * @return int
     */
    public function getCountAlbums()
    {
        return $this->countAlbums;
    }

    /**
     * @return int
     */
    public function getCountTopics()
    {
        return $this->countTopics;
    }

    /**
     * @return int
     */
    public function getCountVideos()
    {
        return $this->countVideos;
    }

    /**
     * @return int
     */
    public function getCountDocs()
    {
        return $this->countDocs;
    }

    /**
     * @return int
     */
    public function getCountRecords()
    {
        return $this->countRecords;
    }
}
