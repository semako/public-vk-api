<?php

namespace semako\vkApi\entity;

use semako\vkApi\interfaces\IMarketAlbum;
use semako\vkApi\traits\WithId;
use semako\vkApi\traits\WithOwnerId;
use semako\vkApi\traits\WithPhoto;
use semako\vkApi\traits\WithTitle;
use semako\yii2Common\traits\ClassName;
use semako\yii2Common\traits\ToArray;

/**
 * Class MarketAlbum
 * @package semako\vkApi\entity
 * @link https://new.vk.com/dev/attachments_w?f=15.%20%D0%9F%D0%BE%D0%B4%D0%B1%D0%BE%D1%80%D0%BA%D0%B0%20%D1%82%D0%BE%D0%B2%D0%B0%D1%80%D0%BE%D0%B2%20(type%20%3D%20market_album)
 */
class MarketAlbum implements IMarketAlbum
{
    use ClassName, ToArray, WithId, WithOwnerId, WithTitle, WithPhoto;

    /**
     * @var int
     */
    private $countItems;

    /**
     * @var int
     */
    private $countPhotos;

    /**
     * @var int
     */
    private $dateUpdated;

    /**
     * @return int
     */
    public function getCountItems()
    {
        return $this->countItems;
    }

    /**
     * @return int
     */
    public function getCountPhotos()
    {
        return $this->countPhotos;
    }

    /**
     * @return int
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }
}
