<?php

namespace semako\vkApi\entity;

use semako\vkApi\interfaces\IGraffiti;
use semako\vkApi\traits\WithId;
use semako\vkApi\traits\WithOwnerId;
use semako\vkApi\traits\WithPhotos;
use semako\yii2Common\traits\ClassName;
use semako\yii2Common\traits\ToArray;

/**
 * Class Graffiti
 * @package semako\vkApi\entity
 * @link https://new.vk.com/dev/attachments_w?f=6.%20%D0%93%D1%80%D0%B0%D1%84%D1%84%D0%B8%D1%82%D0%B8%20(type%20%3D%20graffiti)
 */
class Graffiti implements IGraffiti
{
    use ClassName, ToArray, WithId, WithOwnerId, WithPhotos;
}
