<?php

namespace semako\vkApi\enums;

/**
 * Class AdminLevel
 * @package semako\vkApi\enums
 */
abstract class AdminLevel
{
    const MODER  = 'moder';
    const EDITOR = 'editor';
    const ADMIN  = 'admin';
}
