<?php

namespace semako\vkApi\enums;

/**
 * Class PostType
 * @package semako\vkApi\enums
 */
abstract class PostType
{
    const POST     = 'post';
    const COPY     = 'copy';
    const REPLY    = 'reply';
    const POSTPONE = 'postpone';
    const SUGGEST  = 'suggest';
}
