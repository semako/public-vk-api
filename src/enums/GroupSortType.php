<?php

namespace semako\vkApi\enums;

/**
 * Class GroupSortType
 * @package semako\vkApi\enums
 */
abstract class GroupSortType
{
    const STANDARD          = 0; // сортировать по умолчанию (аналогично результатам поиска в полной версии сайта);
    const SPEED             = 1; // сортировать по скорости роста;
    const ACTIVITY_TO_USERS = 2; // сортировать по отношению дневной посещаемости к количеству пользователей;
    const LIKES_TO_USERS    = 3; // сортировать по отношению количества лайков к количеству пользователей;
    const COMMENTS_TO_USERS = 4; // сортировать по отношению количества комментариев к количеству пользователей;
    const RECORDS_TO_USERS  = 5; // сортировать по отношению количества записей в обсуждениях к количеству пользователей.
}
