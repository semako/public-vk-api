<?php

namespace semako\vkApi\enums;

/**
 * Class ApiResponseFormat
 * @package semako\vkApi\enums
 */
abstract class ApiResponseFormat
{
    const FMT_ARRAY  = 'array';
    const FMT_OBJECT = 'object';
}
