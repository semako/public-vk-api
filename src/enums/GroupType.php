<?php

namespace semako\vkApi\enums;

/**
 * Class GroupType
 * @package semako\vkApi\enums
 */
abstract class GroupType
{
    const GROUP = 'group';
    const PAGE  = 'page';
    const EVENT = 'event';
}
