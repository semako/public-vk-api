<?php

namespace semako\vkApi\enums;

/**
 * Class AttachmentType
 * @package semako\vkApi\enums
 */
abstract class AttachmentType
{
    const PHOTO        = 'photo';
    const POSTED_PHOTO = 'posted_photo';
    const VIDEO        = 'video';
    const AUDIO        = 'audio';
    const DOC          = 'doc';
    const GRAFFITI     = 'graffiti';
    const LINK         = 'link';
    const NOTE         = 'note';
    const APP          = 'app';
    const POLL         = 'poll';
    const PAGE         = 'page';
    const ALBUM        = 'album';
    const PHOTOS_LIST  = 'photos_list';
    const MARKET       = 'market';
    const MARKET_ALBUM = 'market_album';
}
