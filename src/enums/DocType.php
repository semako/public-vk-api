<?php

namespace semako\vkApi\enums;

/**
 * Class DocType
 * @package semako\vkApi\enums
 */
abstract class DocType
{
    const TEXT    = 1;
    const ARCHIVE = 2;
    const GIF     = 3;
    const IMAGE   = 4;
    const AUDIO   = 5;
    const VIDEO   = 6;
    const BOOK    = 7;
    const UNKNOWN = 8;
}
