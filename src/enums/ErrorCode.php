<?php

namespace semako\vkApi\enums;

use yii;

/**
 * Class ErrorCode
 * @package semako\vkApi\enums
 */
abstract class ErrorCode
{
    const ERR_UNKNOWN_ERROR = 1;
    const ERR_APP_DISABLED = 2;
    const ERR_UNKNOWN_METHOD = 3;
    const ERR_INVALID_SIGNATURE = 4;
    const ERR_AUTH_FAILED = 5;
    const ERR_TOO_MANY_REQUESTS = 6;
    const ERR_NO_ACTION_ACCESS = 7;
    const ERR_INCORRECT_QUERY = 8;
    const ERR_TOO_MANY_SIMILAR_ACTIONS = 9;
    const ERR_INTERNAL_SERVER_ERROR = 10;
    const ERR_APP_IN_TEST_MODE = 11;
    const ERR_CAPTCHA_REQUIRED = 14;
    const ERR_ACCESS_FORBIDDEN = 15;
    const ERR_HTTPS_REQUIRED = 16;
    const ERR_USER_VALIDATION_REQUIRED_REDIRECT = 17;
    const ERR_DISALLOWED_FOR_NON_STANDALONE = 20;
    const ERR_STANDALONE_AND_OPEN_API_ALLOWED = 21;
    const ERR_METHOD_DISABLED = 23;
    const ERR_USER_VALIDATION_REQUIRED = 24;
    const ERR_MISSED_REQUIRED_PARAM = 100;
    const ERR_INVALID_API_ID = 101;
    const ERR_INVALID_USER_IDENTITY = 113;
    const ERR_INVALID_TIMESTAMP = 150;
    const ERR_ALBUM_ACCESS_FORBIDDEN = 200;
    const ERR_AUDIO_ACCESS_FORBIDDEN = 201;
    const ERR_GROUP_ACCESS_FORBIDDEN = 203;
    const ERR_ALBUM_IS_FULL = 300;
    const ERR_VOICES_TRANSFER_DISABLED = 500;
    const ERR_ADVERTISING_CABINET_ACCESS_FORBIDDEN = 600;
    const ERR_ADVERTISING_CABINET_ERROR = 603;

    /**
     * @param $key
     * @return null|string
     */
    public static function title($key)
    {
        return isset(self::titles()[$key]) ? self::titles()[$key] : null;
    }

    /**
     * Get title list
     * @return string[]
     */
    public static function titles()
    {
        return [
            self::ERR_UNKNOWN_ERROR => Yii::t('app', 'Произошла неизвестная ошибка. Попробуйте повторить запрос позже.'),
            self::ERR_TOO_MANY_REQUESTS => Yii::t('app', 'Слишком много запросов в секунду. Задайте больший интервал между вызовами или используйте метод execute. Подробнее об ограничениях на частоту вызовов см. на странице http://vk.com/dev/api_requests.'),
            self::ERR_TOO_MANY_SIMILAR_ACTIONS => Yii::t('app', 'Слишком много однотипных действий. Нужно сократить число однотипных обращений. Для более эффективной работы Вы можете использовать execute или JSONP.'),
            self::ERR_INTERNAL_SERVER_ERROR => Yii::t('app', 'Произошла внутренняя ошибка сервера. Попробуйте повторить запрос позже.'),

            self::ERR_INVALID_SIGNATURE => Yii::t('app', 'Неверная подпись. Проверьте правильность формирования подписи запроса: https://vk.com/dev/api_nohttps'),
            self::ERR_AUTH_FAILED => Yii::t('app', 'Авторизация пользователя не удалась. Убедитесь, что Вы используете верную схему авторизации. Для работы с методами без префикса secure Вам нужно авторизовать пользователя одним из этих способов: http://vk.com/dev/auth_sites, http://vk.com/dev/auth_mobile.'),
            self::ERR_NO_ACTION_ACCESS => Yii::t('app', 'Нет прав для выполнения этого действия. Проверьте, получены ли нужные права доступа при авторизации. Это можно сделать с помощью метода account.getAppPermissions.'),
            self::ERR_CAPTCHA_REQUIRED => Yii::t('app', 'Требуется ввод кода с картинки (Captcha). Процесс обработки этой ошибки подробно описан на отдельной странице.'),
            self::ERR_ACCESS_FORBIDDEN => Yii::t('app', 'Доступ запрещён. Убедитесь, что Вы используете верные идентификаторы, и доступ к контенту для текущего пользователя есть в полной версии сайта.'),
            self::ERR_USER_VALIDATION_REQUIRED_REDIRECT => Yii::t('app', 'Требуется валидация пользователя. Действие требует подтверждения — необходимо перенаправить пользователя на служебную страницу для валидации.'),
            self::ERR_USER_VALIDATION_REQUIRED => Yii::t('app', 'Требуется подтверждение со стороны пользователя.'),
            self::ERR_ALBUM_ACCESS_FORBIDDEN => Yii::t('app', 'Доступ к альбому запрещён. Убедитесь, что Вы используете верные идентификаторы (для пользователей owner_id положительный, для сообществ — отрицательный), и доступ к запрашиваемому контенту для текущего пользователя есть в полной версии сайта.'),
            self::ERR_AUDIO_ACCESS_FORBIDDEN => Yii::t('app', 'Доступ к аудио запрещён. Убедитесь, что Вы используете верные идентификаторы (для пользователей owner_id положительный, для сообществ — отрицательный), и доступ к запрашиваемому контенту для текущего пользователя есть в полной версии сайта.'),
            self::ERR_GROUP_ACCESS_FORBIDDEN => Yii::t('app', 'Доступ к группе запрещён. Убедитесь, что текущий пользователь является участником или руководителем сообщества (для закрытых и частных групп и встреч).'),
            self::ERR_ADVERTISING_CABINET_ACCESS_FORBIDDEN => Yii::t('app', 'Нет прав на выполнение данных операций с рекламным кабинетом.'),

            self::ERR_INVALID_USER_IDENTITY => Yii::t('app', 'Неверный идентификатор пользователя. Убедитесь, что Вы используете верный идентификатор. Получить ID по короткому имени можно методом utils.resolveScreenName.'),
            self::ERR_APP_DISABLED => Yii::t('app', 'Приложение выключено. Необходимо включить приложение в настройках https://vk.com/editapp?id={Ваш API_ID} или использовать тестовый режим (test_mode=1)'),
            self::ERR_UNKNOWN_METHOD => Yii::t('app', 'Передан неизвестный метод. Проверьте, правильно ли указано название вызываемого метода: http://vk.com/dev/methods.'),
            self::ERR_INCORRECT_QUERY => Yii::t('app', 'Неверный запрос. Проверьте синтаксис запроса и список используемых параметров (его можно найти на странице с описанием метода).'),
            self::ERR_APP_IN_TEST_MODE => Yii::t('app', 'В тестовом режиме приложение должно быть выключено или пользователь должен быть залогинен. Выключите приложение в настройках https://vk.com/editapp?id={Ваш API_ID}'),
            self::ERR_HTTPS_REQUIRED => Yii::t('app', 'Требуется выполнение запросов по протоколу HTTPS, т.к. пользователь включил настройку, требующую работу через безопасное соединение. Чтобы избежать появления такой ошибки, в Standalone-приложении Вы можете предварительно проверять состояние этой настройки у пользователя методом account.getInfo.'),
            self::ERR_DISALLOWED_FOR_NON_STANDALONE => Yii::t('app', 'Данное действие запрещено для не Standalone приложений. Если ошибка возникает несмотря на то, что Ваше приложение имеет тип Standalone, убедитесь, что при авторизации Вы используете redirect_uri=https://oauth.vk.com/blank.html. Подробнее см. http://vk.com/dev/auth_mobile.'),
            self::ERR_STANDALONE_AND_OPEN_API_ALLOWED => Yii::t('app', 'Данное действие разрешено только для Standalone и Open API приложений.'),
            self::ERR_METHOD_DISABLED => Yii::t('app', 'Метод был выключен. Все актуальные методы ВК API, которые доступны в настоящий момент, перечислены здесь: http://vk.com/dev/methods.'),
            self::ERR_MISSED_REQUIRED_PARAM => Yii::t('app', 'Один из необходимых параметров был не передан или неверен. Проверьте список требуемых параметров и их формат на странице с описанием метода.'),
            self::ERR_INVALID_API_ID => Yii::t('app', 'Неверный API ID приложения. Найдите приложение в списке администрируемых на странице http://vk.com/apps?act=settings и укажите в запросе верный API_ID (идентификатор приложения).'),
            self::ERR_INVALID_TIMESTAMP => Yii::t('app', 'Неверный timestamp. Получить актуальное значение Вы можете методом utils.getServerTime.'),
            self::ERR_ALBUM_IS_FULL => Yii::t('app', 'Альбом переполнен. Перед продолжением работы нужно удалить лишние объекты из альбома или использовать другой альбом.'),
            self::ERR_VOICES_TRANSFER_DISABLED => Yii::t('app', 'Действие запрещено. Вы должны включить переводы голосов в настройках приложения.Проверьте настройки приложения: http://vk.com/editapp?id={Ваш API_ID}&section=payments'),
            self::ERR_ADVERTISING_CABINET_ERROR => Yii::t('app', 'Произошла ошибка при работе с рекламным кабинетом.'),
        ];
    }
}
