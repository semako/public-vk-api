<?php

namespace semako\vkApi\enums;

/**
 * Class ApiVersion
 * @package semako\vkApi\enums
 */
abstract class ApiVersion
{
    const VERSION = '5.52';
}