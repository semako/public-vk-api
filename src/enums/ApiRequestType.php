<?php

namespace semako\vkApi\enums;

/**
 * Class ApiRequestType
 * @package semako\vkApi\enums
 */
abstract class ApiRequestType
{
    const GET  = 'get';
    const POST = 'post';
}
