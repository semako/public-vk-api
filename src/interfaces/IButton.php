<?php

namespace semako\vkApi\interfaces;

use semako\vkApi\interfaces\common\IToArray;
use semako\vkApi\interfaces\common\IWithTitle;
use semako\vkApi\interfaces\common\IWithUrl;

/**
 * Interface IButton
 * @package semako\vkApi\interfaces
 * @link https://new.vk.com/dev/link_button
 */
interface IButton extends
    IToArray,
    IWithTitle,
    IWithUrl
{
}
