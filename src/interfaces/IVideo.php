<?php

namespace semako\vkApi\interfaces;

use semako\vkApi\interfaces\common\IToArray;
use semako\vkApi\interfaces\common\IWithDate;
use semako\vkApi\interfaces\common\IWithDescription;
use semako\vkApi\interfaces\common\IWithId;
use semako\vkApi\interfaces\common\IWithOwnerId;
use semako\vkApi\interfaces\common\IWithPhotos;
use semako\vkApi\interfaces\common\IWithTitle;

/**
 * Interface IVideo
 * @package semako\vkApi\interfaces
 * @link https://new.vk.com/dev/video_object
 */
interface IVideo extends
    IToArray,
    IWithId,
    IWithOwnerId,
    IWithTitle,
    IWithDescription,
    IWithPhotos,
    IWithDate
{
    /**
     * @return int
     */
    public function getDuration();

    /**
     * @return int|null
     */
    public function getDateAdded();

    /**
     * @return int
     */
    public function getCountViews();

    /**
     * @return int
     */
    public function getCountComments();

    /**
     * @return string
     */
    public function getPlayerUrl();

    /**
     * @return string|null
     */
    public function getAccessKey();

    /**
     * @return bool
     */
    public function isProcessing();

    /**
     * @return bool
     */
    public function isLive();
}
