<?php

namespace semako\vkApi\interfaces;

use semako\vkApi\interfaces\common\IToArray;
use semako\vkApi\interfaces\common\IWithApplication;
use semako\vkApi\interfaces\common\IWithButton;
use semako\vkApi\interfaces\common\IWithDescription;
use semako\vkApi\interfaces\common\IWithPhoto;
use semako\vkApi\interfaces\common\IWithProduct;
use semako\vkApi\interfaces\common\IWithRating;
use semako\vkApi\interfaces\common\IWithTitle;
use semako\vkApi\interfaces\common\IWithUrl;

/**
 * Interface ILink
 * @package semako\vkApi\interfaces
 * @link https://new.vk.com/dev/attachments_w?f=7.%20%D0%A1%D1%81%D1%8B%D0%BB%D0%BA%D0%B0%20(type%20%3D%20link)
 */
interface ILink extends
    IToArray,
    IWithUrl,
    IWithTitle,
    IWithDescription,
    IWithPhoto,
    IWithProduct,
    IWithRating,
    IWithApplication,
    IWithButton
{
    /**
     * @return string|null
     */
    public function getCaption();

    /**
     * @return bool
     */
    public function isExternal();

    /**
     * @return string|null
     */
    public function getPreviewPage();

    /**
     * @return string|null
     */
    public function getPreviewUrl();
}
