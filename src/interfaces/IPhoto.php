<?php

namespace semako\vkApi\interfaces;

use semako\vkApi\interfaces\common\IToArray;
use semako\vkApi\interfaces\common\IWithDate;
use semako\vkApi\interfaces\common\IWithId;
use semako\vkApi\interfaces\common\IWithOwnerId;
use semako\vkApi\interfaces\common\IWithPhotos;
use semako\vkApi\interfaces\common\IWithText;

/**
 * Interface IPhoto
 * @package semako\vkApi\interfaces
 * @link https://new.vk.com/dev/photo
 */
interface IPhoto extends
    IToArray,
    IWithId,
    IWithOwnerId,
    IWithText,
    IWithDate,
    IWithPhotos
{
    /**
     * @return int
     */
    public function getAlbumId();

    /**
     * @return int|null
     */
    public function getUserId();

    /**
     * @return int|null
     */
    public function getOriginalWidth();

    /**
     * @return int|null
     */
    public function getOriginalHeight();
}
