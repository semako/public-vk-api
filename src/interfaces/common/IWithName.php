<?php

namespace semako\vkApi\interfaces\common;

/**
 * Interface IWithName
 * @package semako\vkApi\interfaces\common
 */
interface IWithName
{
    /**
     * @return string
     */
    public function getName();
}
