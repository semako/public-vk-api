<?php

namespace semako\vkApi\interfaces\common;

/**
 * Interface IWithDescription
 * @package semako\vkApi\interfaces\common
 */
interface IWithDescription
{
    /**
     * @return string
     */
    public function getDescription();
}
