<?php

namespace semako\vkApi\interfaces\common;

use semako\vkApi\interfaces\IPhoto;

/**
 * Interface IWithPhoto
 * @package semako\vkApi\interfaces\common
 */
interface IWithPhoto
{
    /**
     * @return IPhoto|null
     */
    public function getPhoto();
}
