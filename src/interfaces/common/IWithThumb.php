<?php

namespace semako\vkApi\interfaces\common;

use semako\vkApi\interfaces\IPhoto;

/**
 * Interface IWithThumb
 * @package semako\vkApi\interfaces\common
 */
interface IWithThumb
{
    /**
     * @return IPhoto|null
     */
    public function getThumb();
}
