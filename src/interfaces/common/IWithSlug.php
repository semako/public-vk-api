<?php

namespace semako\vkApi\interfaces\common;

/**
 * Interface IWithSlug
 * @package semako\vkApi\interfaces\common
 */
interface IWithSlug
{
    /**
     * @return string
     */
    public function getSlug();
}
