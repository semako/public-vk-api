<?php

namespace semako\vkApi\interfaces\common;

use semako\vkApi\interfaces\IApplication;

/**
 * Interface IWithApplication
 * @package semako\vkApi\interfaces\common
 */
interface IWithApplication
{
    /**
     * @return IApplication|null
     */
    public function getApplication();
}
