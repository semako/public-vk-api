<?php

namespace semako\vkApi\interfaces\common;

/**
 * Interface IWithPhotos
 * @package semako\vkApi\interfaces\common
 */
interface IWithPhotos
{
    /**
     * @return array
     */
    public function getPhotos();

    /**
     * @return string|null
     */
    public function getLargestPhoto();

    /**
     * @return string|null
     */
    public function getSmallestPhoto();
}
