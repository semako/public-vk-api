<?php

namespace semako\vkApi\interfaces\common;

/**
 * Interface IToArray
 * @package semako\vkApi\interfaces\common
 */
interface IToArray
{
    /**
     * @return array
     */
    public function toArray();
}
