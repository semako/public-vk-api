<?php

namespace semako\vkApi\interfaces\common;

use semako\vkApi\interfaces\IButton;

/**
 * Interface IWithButton
 * @package semako\vkApi\interfaces\common
 */
interface IWithButton
{
    /**
     * @return IButton|null
     */
    public function getButton();
}
