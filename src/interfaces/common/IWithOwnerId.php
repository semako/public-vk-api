<?php

namespace semako\vkApi\interfaces\common;

/**
 * Interface IWithOwnerId
 * @package semako\vkApi\interfaces\common
 */
interface IWithOwnerId
{
    /**
     * @return int
     */
    public function getOwnerId();
}
