<?php

namespace semako\vkApi\interfaces\common;

/**
 * Interface IWithDate
 * @package semako\vkApi\interfaces\common
 */
interface IWithDate
{
    /**
     * @return int
     */
    public function getDate();
}
