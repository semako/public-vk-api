<?php

namespace semako\vkApi\interfaces\common;

use semako\vkApi\interfaces\IProduct;

/**
 * Interface IWithProduct
 * @package semako\vkApi\interfaces\common
 */
interface IWithProduct
{
    /**
     * @return IProduct|null
     */
    public function getProduct();
}
