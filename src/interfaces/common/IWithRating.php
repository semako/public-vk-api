<?php

namespace semako\vkApi\interfaces\common;

use semako\vkApi\interfaces\IRating;

/**
 * Interface IWithRating
 * @package semako\vkApi\interfaces\common
 */
interface IWithRating
{
    /**
     * @return IRating|null
     */
    public function getRating();
}
