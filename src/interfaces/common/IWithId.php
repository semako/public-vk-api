<?php

namespace semako\vkApi\interfaces\common;

/**
 * Interface IWithId
 * @package semako\vkApi\interfaces\common
 */
interface IWithId
{
    /**
     * @return int
     */
    public function getId();
}
