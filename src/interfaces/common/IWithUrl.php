<?php

namespace semako\vkApi\interfaces\common;

/**
 * Interface IWithUrl
 * @package semako\vkApi\interfaces\common
 */
interface IWithUrl
{
    /**
     * @return string
     */
    public function getUrl();
}
