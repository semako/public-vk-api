<?php

namespace semako\vkApi\interfaces\common;

/**
 * Interface IWithText
 * @package semako\vkApi\interfaces\common
 */
interface IWithText
{
    /**
     * @return string|null
     */
    public function getText();
}
