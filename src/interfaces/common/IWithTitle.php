<?php

namespace semako\vkApi\interfaces\common;

/**
 * Interface IWithTitle
 * @package semako\vkApi\interfaces\common
 */
interface IWithTitle
{
    /**
     * @return string
     */
    public function getTitle();
}
