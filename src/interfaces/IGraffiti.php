<?php

namespace semako\vkApi\interfaces;

use semako\vkApi\interfaces\common\IToArray;
use semako\vkApi\interfaces\common\IWithId;
use semako\vkApi\interfaces\common\IWithOwnerId;
use semako\vkApi\interfaces\common\IWithPhotos;

/**
 * Interface IGraffiti
 * @package semako\vkApi\interfaces
 * @link https://new.vk.com/dev/attachments_w?f=6.%20%D0%93%D1%80%D0%B0%D1%84%D1%84%D0%B8%D1%82%D0%B8%20(type%20%3D%20graffiti)
 */
interface IGraffiti extends
    IToArray,
    IWithId,
    IWithOwnerId,
    IWithPhotos
{
}
