<?php

namespace semako\vkApi\interfaces;

use semako\vkApi\interfaces\common\IToArray;
use semako\vkApi\interfaces\common\IWithDate;
use semako\vkApi\interfaces\common\IWithId;
use semako\vkApi\interfaces\common\IWithOwnerId;

/**
 * Interface IPoll
 * @package semako\vkApi\interfaces
 * @link https://new.vk.com/dev/attachments_w?f=10.%20%D0%9E%D0%BF%D1%80%D0%BE%D1%81%20(type%20%3D%20poll)
 */
interface IPoll extends
    IToArray,
    IWithId,
    IWithOwnerId,
    IWithDate
{
    /**
     * @return string
     */
    public function getQuestion();

    /**
     * @return int
     */
    public function getCountVotes();

    /**
     * @return int|null
     */
    public function getUserChoice();

    /**
     * @return IAnswer[]
     */
    public function getAnswers();

    /**
     * @return bool
     */
    public function isAnonymous();
}
