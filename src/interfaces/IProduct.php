<?php

namespace semako\vkApi\interfaces;

use semako\vkApi\interfaces\common\IToArray;

/**
 * Interface IProduct
 * @package semako\vkApi\interfaces
 * @link https://new.vk.com/dev/link_product
 */
interface IProduct extends IToArray
{
    /**
     * @return IPrice
     */
    public function getPrice();
}
