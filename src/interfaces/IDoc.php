<?php

namespace semako\vkApi\interfaces;

use semako\vkApi\enums\DocType;
use semako\vkApi\interfaces\common\IToArray;
use semako\vkApi\interfaces\common\IWithDate;
use semako\vkApi\interfaces\common\IWithId;
use semako\vkApi\interfaces\common\IWithPhotos;
use semako\vkApi\interfaces\common\IWithTitle;
use semako\vkApi\interfaces\common\IWithUrl;

/**
 * Interface IDoc
 * @package semako\vkApi\interfaces
 * @link https://new.vk.com/dev/doc
 */
interface IDoc extends
    IToArray,
    IWithId,
    IWithTitle,
    IWithUrl,
    IWithDate,
    IWithPhotos
{
    /**
     * @return int
     */
    public function getFileSize();

    /**
     * @return string
     */
    public function getExt();

    /**
     * @return int
     * @see DocType
     */
    public function getType();
}
