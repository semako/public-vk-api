<?php

namespace semako\vkApi\interfaces;

use semako\vkApi\interfaces\common\IToArray;
use semako\vkApi\interfaces\common\IWithDate;
use semako\vkApi\interfaces\common\IWithId;
use semako\vkApi\interfaces\common\IWithOwnerId;
use semako\vkApi\interfaces\common\IWithText;
use semako\vkApi\interfaces\common\IWithTitle;

/**
 * Interface INote
 * @package semako\vkApi\interfaces
 * @link https://new.vk.com/dev/note
 */
interface INote extends
    IToArray,
    IWithId,
    IWithOwnerId,
    IWithTitle,
    IWithText,
    IWithDate
{
    /**
     * @return int
     */
    public function getCountComments();

    /**
     * @return int
     */
    public function getCountReadComments();

    /**
     * @return string
     */
    public function getViewUrl();
}
