<?php

namespace semako\vkApi\interfaces;

use semako\vkApi\interfaces\common\IToArray;
use semako\vkApi\interfaces\common\IWithId;
use semako\vkApi\interfaces\common\IWithName;
use semako\vkApi\interfaces\common\IWithPhotos;
use semako\vkApi\interfaces\common\IWithSlug;

/**
 * Interface IGroup
 * @package semako\vkApi\interfaces
 * @link https://new.vk.com/dev/group
 */
interface IGroup extends
    IToArray,
    IWithId,
    IWithName,
    IWithSlug,
    IWithPhotos
{
    /**
     * @return boolean
     */
    public function isClosed();

    /**
     * @return boolean
     */
    public function isAdmin();

    /**
     * @return boolean
     */
    public function isMember();

    /**
     * @return boolean
     */
    public function isCanPost();

    /**
     * @return boolean
     */
    public function isCanSeeAllPosts();

    /**
     * @return boolean
     */
    public function isVerified();

    /**
     * @return string
     */
    public function getType();

    /**
     * @return null|string
     */
    public function getSite();

    /**
     * @return ICountry|null
     */
    public function getCountry();

    /**
     * @return ICity|null
     */
    public function getCity();

    /**
     * @return null|string
     */
    public function getDescription();

    /**
     * @return int
     */
    public function getMembers();

    /**
     * @return null|string
     */
    public function getStatus();

    /**
     * @return int
     */
    public function getCountPhotos();

    /**
     * @return int
     */
    public function getCountAlbums();

    /**
     * @return int
     */
    public function getCountTopics();

    /**
     * @return int
     */
    public function getCountVideos();

    /**
     * @return int
     */
    public function getCountDocs();

    /**
     * @return int
     */
    public function getCountRecords();
}
