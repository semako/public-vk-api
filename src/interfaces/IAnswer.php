<?php

namespace semako\vkApi\interfaces;

use semako\vkApi\interfaces\common\IToArray;
use semako\vkApi\interfaces\common\IWithId;
use semako\vkApi\interfaces\common\IWithText;

/**
 * Interface IAnswer
 * @package semako\vkApi\interfaces
 * @link https://new.vk.com/dev/attachments_w?f=10.%20%D0%9E%D0%BF%D1%80%D0%BE%D1%81%20(type%20%3D%20poll)
 */
interface IAnswer extends
    IToArray,
    IWithId,
    IWithText
{
    /**
     * @return int
     */
    public function getCountVotes();

    /**
     * @return int
     */
    public function getRating();
}
