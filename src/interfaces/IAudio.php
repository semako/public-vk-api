<?php

namespace semako\vkApi\interfaces;

use semako\vkApi\interfaces\common\IToArray;
use semako\vkApi\interfaces\common\IWithDate;
use semako\vkApi\interfaces\common\IWithId;
use semako\vkApi\interfaces\common\IWithOwnerId;
use semako\vkApi\interfaces\common\IWithTitle;
use semako\vkApi\interfaces\common\IWithUrl;

/**
 * Interface IAudio
 * @package semako\vkApi\interfaces
 * @link https://new.vk.com/dev/audio_object
 */
interface IAudio extends
    IToArray,
    IWithId,
    IWithOwnerId,
    IWithTitle,
    IWithDate,
    IWithUrl
{
    /**
     * @return string
     */
    public function getArtist();

    /**
     * @return int
     */
    public function getDuration();

    /**
     * @return int|null
     */
    public function getLyricsId();

    /**
     * @return int|null
     */
    public function getAlbumId();

    /**
     * @return int|null
     */
    public function getGenreId();

    /**
     * @return bool
     */
    public function isNoSearch();
}
