<?php

namespace semako\vkApi\interfaces;

use semako\vkApi\interfaces\common\IToArray;

/**
 * Interface IPhotosList
 * @package semako\vkApi\interfaces
 * @link https://new.vk.com/dev/attachments_w?f=13.%20%D0%A1%D0%BF%D0%B8%D1%81%D0%BE%D0%BA%20%D1%84%D0%BE%D1%82%D0%BE%D0%B3%D1%80%D0%B0%D1%84%D0%B8%D0%B9%20(type%3Dphotos_list)
 */
interface IPhotosList extends IToArray
{
    /**
     * @return IPhoto[]
     */
    public function getPhotos();
}
