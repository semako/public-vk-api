<?php

namespace semako\vkApi\interfaces;

use semako\vkApi\enums\PostType;
use semako\vkApi\interfaces\common\IToArray;
use semako\vkApi\interfaces\common\IWithDate;
use semako\vkApi\interfaces\common\IWithId;
use semako\vkApi\interfaces\common\IWithOwnerId;
use semako\vkApi\interfaces\common\IWithText;

/**
 * Interface IPost
 * @package semako\vkApi\interfaces
 * @link https://new.vk.com/dev/post
 */
interface IPost extends
    IToArray,
    IWithId,
    IWithDate,
    IWithText,
    IWithOwnerId
{
    /**
     * @return int
     */
    public function getFromId();

    /**
     * @return int|null
     */
    public function getReplyOwnerId();

    /**
     * @return int|null
     */
    public function getReplyPostId();

    /**
     * @return int
     */
    public function getCountComments();

    /**
     * @return int
     */
    public function getCountLikes();

    /**
     * @return int
     */
    public function getCountReposts();

    /**
     * @return int
     */
    public function getCountPhotos();

    /**
     * @return int
     */
    public function getCountVideos();

    /**
     * @return string
     * @see PostType
     */
    public function getPostType();

    /**
     * @return object[]
     */
    public function getAttachments();

    /**
     * @return int|null
     */
    public function getSignerId();

    /**
     * @return bool
     */
    public function isCanPin();

    /**
     * @return bool
     */
    public function isCanDelete();

    /**
     * @return bool
     */
    public function isCanEdit();

    /**
     * @return bool
     */
    public function isPinned();
}
