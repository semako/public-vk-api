<?php

namespace semako\vkApi\interfaces;

use semako\vkApi\interfaces\common\IToArray;
use semako\vkApi\interfaces\common\IWithDescription;
use semako\vkApi\interfaces\common\IWithId;
use semako\vkApi\interfaces\common\IWithOwnerId;
use semako\vkApi\interfaces\common\IWithThumb;
use semako\vkApi\interfaces\common\IWithTitle;

/**
 * Interface IAlbum
 * @package semako\vkApi\interfaces
 * @link https://new.vk.com/dev/attachments_w?f=12.%20%D0%90%D0%BB%D1%8C%D0%B1%D0%BE%D0%BC%20%D1%81%20%D1%84%D0%BE%D1%82%D0%BE%D0%B3%D1%80%D0%B0%D1%84%D0%B8%D1%8F%D0%BC%D0%B8%20(type%3Dalbum)
 */
interface IAlbum extends
    IToArray,
    IWithId,
    IWithThumb,
    IWithOwnerId,
    IWithTitle,
    IWithDescription
{
    /**
     * @return int
     */
    public function getDateCreated();

    /**
     * @return int|null
     */
    public function getDateUpdated();

    /**
     * @return int
     */
    public function getCountPhotos();
}
