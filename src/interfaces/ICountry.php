<?php

namespace semako\vkApi\interfaces;

use semako\vkApi\interfaces\common\IToArray;
use semako\vkApi\interfaces\common\IWithId;
use semako\vkApi\interfaces\common\IWithTitle;

/**
 * Interface ICountry
 * @package semako\vkApi\interfaces
 */
interface ICountry extends
    IToArray,
    IWithId,
    IWithTitle
{
}