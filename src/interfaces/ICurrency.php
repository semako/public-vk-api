<?php

namespace semako\vkApi\interfaces;

use semako\vkApi\interfaces\common\IToArray;
use semako\vkApi\interfaces\common\IWithId;
use semako\vkApi\interfaces\common\IWithName;

/**
 * Interface ICurrency
 * @package semako\vkApi\interfaces
 * @link https://new.vk.com/dev/price
 */
interface ICurrency extends
    IToArray,
    IWithId,
    IWithName
{
}
