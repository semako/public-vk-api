<?php

namespace semako\vkApi\interfaces;

use semako\vkApi\interfaces\common\IToArray;

/**
 * Interface IRating
 * @package semako\vkApi\interfaces
 * @link https://new.vk.com/dev/link_rating
 */
interface IRating extends IToArray
{
    /**
     * @return int
     */
    public function getStars();

    /**
     * @return int
     */
    public function getCountReviews();
}
