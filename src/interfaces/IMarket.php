<?php

namespace semako\vkApi\interfaces;

use semako\vkApi\interfaces\common\IToArray;

/**
 * Interface IMarket
 * @package semako\vkApi\interfaces
 * @link https://new.vk.com/dev/item
 */
interface IMarket extends IToArray
{
}