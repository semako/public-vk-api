<?php

namespace semako\vkApi\interfaces;

use semako\vkApi\interfaces\common\IToArray;
use semako\vkApi\interfaces\common\IWithId;
use semako\vkApi\interfaces\common\IWithTitle;

/**
 * Interface ICity
 * @package semako\vkApi\interfaces
 */
interface ICity extends
    IToArray,
    IWithId,
    IWithTitle
{
}
