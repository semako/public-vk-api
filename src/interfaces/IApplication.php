<?php

namespace semako\vkApi\interfaces;

use semako\vkApi\interfaces\common\IToArray;
use semako\vkApi\interfaces\common\IWithId;
use semako\vkApi\interfaces\common\IWithName;

/**
 * Interface IApplication
 * @package semako\vkApi\interfaces
 * @link https://new.vk.com/dev/attachments_w?f=9.%20%D0%9A%D0%BE%D0%BD%D1%82%D0%B5%D0%BD%D1%82%20%D0%BF%D1%80%D0%B8%D0%BB%D0%BE%D0%B6%D0%B5%D0%BD%D0%B8%D1%8F%20(type%20%3D%20app)
 */
interface IApplication extends
    IToArray,
    IWithId,
    IWithName
{
    /**
     * @return string
     */
    public function getThumbPhotoUrl();

    /**
     * @return string
     */
    public function getPhotoUrl();
}
