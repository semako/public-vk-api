<?php

namespace semako\vkApi\interfaces;

use semako\vkApi\enums\ApiRequestType;
use semako\vkApi\enums\ApiResponseFormat;
use VK\VKException;

/**
 * Interface IVkApi
 * @package semako\vkApi\interfaces
 */
interface IVkApi
{
    /**
     * @return string
     */
    public function getVersion();

    /**
     * @return string
     */
    public function getApiSecret();

    /**
     * @return int
     */
    public function getAppId();

    /**
     * Set special API version.
     * @param int $version
     * @return void
     */
    public function setApiVersion($version);

    /**
     * @param $accessToken
     * @return IVkApi
     */
    public function withToken($accessToken);

    /**
     * Set Access Token.
     * @param string $accessToken
     * @throws VKException
     * @return void
     */
    public function setAccessToken($accessToken);

    /**
     * Returns base API url.
     * @param string $method
     * @return string
     */
    public function getApiUrl($method);

    /**
     * Returns authorization link with passed parameters.
     * @param string $apiSettings
     * @param string $callbackUrl
     * @param bool $testMode
     * @return string
     */
    public function getAuthorizeUrl($apiSettings = '', $callbackUrl = 'https://api.vk.com/blank.html', $testMode = false);

    /**
     * Returns access token by code received on authorization link.
     * @param string $code
     * @param string $callbackUrl
     * @throws VKException
     * @return array
     */
    public function getAccessToken($code, $callbackUrl = 'https://api.vk.com/blank.html');

    /**
     * Return user authorization status.
     * @return bool
     */
    public function isAuth();

    /**
     * Check for validity access token.
     * @param string $accessToken
     * @return bool
     */
    public function checkAccessToken($accessToken = null);

    /**
     * Execute API method with parameters and return result.
     * @param string $method
     * @param array $parameters
     * @param string $format
     * @param string $requestMethod
     * @return array|object
     * @see ApiResponseFormat
     * @see ApiRequestType
     */
    public function api($method, $parameters = [], $format = ApiResponseFormat::FMT_ARRAY, $requestMethod = ApiRequestType::GET);
}
