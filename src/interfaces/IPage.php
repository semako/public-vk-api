<?php

namespace semako\vkApi\interfaces;

use semako\vkApi\interfaces\common\IToArray;

/**
 * Interface IPage
 * @package semako\vkApi\interfaces
 * @link https://new.vk.com/dev/page
 */
interface IPage extends IToArray
{
}
