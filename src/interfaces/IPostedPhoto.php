<?php

namespace semako\vkApi\interfaces;

use semako\vkApi\interfaces\common\IToArray;
use semako\vkApi\interfaces\common\IWithId;
use semako\vkApi\interfaces\common\IWithOwnerId;
use semako\vkApi\interfaces\common\IWithPhotos;

/**
 * Interface IPostedPhoto
 * @package semako\vkApi\interfaces
 * @link https://new.vk.com/dev/attachments_w?f=2.%20%D0%A4%D0%BE%D1%82%D0%BE%D0%B3%D1%80%D0%B0%D1%84%D0%B8%D1%8F%2C%20%D0%B7%D0%B0%D0%B3%D1%80%D1%83%D0%B6%D0%B5%D0%BD%D0%BD%D0%B0%D1%8F%20%D0%BD%D0%B0%D0%BF%D1%80%D1%8F%D0%BC%D1%83%D1%8E%20(type%20%3D%20posted_photo)
 */
interface IPostedPhoto extends
    IToArray,
    IWithId,
    IWithOwnerId,
    IWithPhotos
{
}
