<?php

namespace semako\vkApi\interfaces;

use semako\vkApi\interfaces\common\IToArray;
use semako\vkApi\interfaces\common\IWithId;
use semako\vkApi\interfaces\common\IWithOwnerId;
use semako\vkApi\interfaces\common\IWithPhoto;
use semako\vkApi\interfaces\common\IWithTitle;

/**
 * Interface IMarketAlbum
 * @package semako\vkApi\interfaces
 * @link https://new.vk.com/dev/attachments_w?f=15.%20%D0%9F%D0%BE%D0%B4%D0%B1%D0%BE%D1%80%D0%BA%D0%B0%20%D1%82%D0%BE%D0%B2%D0%B0%D1%80%D0%BE%D0%B2%20(type%20%3D%20market_album)
 */
interface IMarketAlbum extends
    IToArray,
    IWithId,
    IWithOwnerId,
    IWithTitle,
    IWithPhoto
{
    /**
     * @return int
     */
    public function getCountItems();

    /**
     * @return int
     */
    public function getCountPhotos();

    /**
     * @return int
     */
    public function getDateUpdated();
}
