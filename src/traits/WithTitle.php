<?php

namespace semako\vkApi\traits;

/**
 * Class WithTitle
 * @package semako\vkApi\traits
 */
trait WithTitle
{
    /**
     * @var string
     */
    private $title;

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
}
