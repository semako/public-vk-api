<?php

namespace semako\vkApi\traits;

/**
 * Class WithText
 * @package semako\vkApi\traits
 */
trait WithText
{
    /**
     * @var string
     */
    private $text;

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }
}
