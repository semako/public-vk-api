<?php

namespace semako\vkApi\traits;

use semako\vkApi\interfaces\IVkApi;
use yii;

/**
 * Class VkApi
 * @package semako\vkApi\traits
 * @property IVkApi $vkApi
 */
trait VkApi
{
    /**
     * @return IVkApi
     */
    public function getVkApi()
    {
        return Yii::$app->vkApi;
    }
}
