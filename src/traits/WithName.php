<?php

namespace semako\vkApi\traits;

/**
 * Class WithName
 * @package semako\vkApi\traits
 */
trait WithName
{
    /**
     * @var string
     */
    private $name;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
