<?php

namespace semako\vkApi\traits;

/**
 * Class WithUrl
 * @package semako\vkApi\traits
 */
trait WithUrl
{
    /**
     * @var string
     */
    private $url;

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }
}
