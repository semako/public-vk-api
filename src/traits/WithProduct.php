<?php

namespace semako\vkApi\traits;

use semako\vkApi\interfaces\IProduct;

/**
 * Class WithProduct
 * @package semako\vkApi\traits
 */
trait WithProduct
{
    /**
     * @var IProduct|null
     */
    private $product;

    /**
     * @return IProduct|null
     */
    public function getProduct()
    {
        return $this->product;
    }
}
