<?php

namespace semako\vkApi\traits;

/**
 * Class WithSlug
 * @package semako\vkApi\traits
 */
trait WithSlug
{
    /**
     * @var string
     */
    private $slug;

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
}
