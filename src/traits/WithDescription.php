<?php

namespace semako\vkApi\traits;

/**
 * Class WithDescription
 * @package semako\vkApi\traits
 */
trait WithDescription
{
    /**
     * @var string|null
     */
    private $description;

    /**
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }
}
