<?php

namespace semako\vkApi\traits;
use semako\vkApi\interfaces\IPhoto;

/**
 * Class WithThumb
 * @package semako\vkApi\traits
 */
trait WithThumb
{
    /**
     * @var IPhoto|null
     */
    private $thumb;

    /**
     * @return IPhoto|null
     */
    public function getThumb()
    {
        return $this->thumb;
    }
}
