<?php

namespace semako\vkApi\traits;

/**
 * Class WithPhotos
 * @package semako\vkApi\traits
 */
trait WithPhotos
{
    /**
     * @var array
     */
    private $photos;

    /**
     * @param array $data
     */
    private function parsePhotos(array $data)
    {
        foreach ($data as $k => $v) {
            if (preg_match('/^photo_([0-9]+)/is', $k, $match)) {
                $this->photos[$match[1]] = $v;
            }
        }
    }

    /**
     * @return array
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * @return string|null
     */
    public function getLargestPhoto()
    {
        if (!sizeof($this->photos)) {
            return null;
        }

        return $this->photos[max(array_keys($this->photos))];
    }

    /**
     * @return string|null
     */
    public function getSmallestPhoto()
    {
        if (!sizeof($this->photos)) {
            return null;
        }

        return $this->photos[min(array_keys($this->photos))];
    }
}
