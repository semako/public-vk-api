<?php

namespace semako\vkApi\traits;

use semako\vkApi\interfaces\IPhoto;

/**
 * Class WithPhoto
 * @package semako\vkApi\traits
 */
trait WithPhoto
{
    /**
     * @var IPhoto|null
     */
    private $photo;

    /**
     * @return IPhoto|null
     */
    public function getPhoto()
    {
        return $this->photo;
    }
}
