<?php

namespace semako\vkApi\traits;

/**
 * Class WithOwnerId
 * @package semako\vkApi\traits
 */
trait WithOwnerId
{
    /**
     * @var int
     */
    private $ownerId;

    /**
     * @return int
     */
    public function getOwnerId()
    {
        return $this->ownerId;
    }
}
