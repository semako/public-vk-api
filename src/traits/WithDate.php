<?php

namespace semako\vkApi\traits;

/**
 * Class WithDate
 * @package semako\vkApi\traits
 */
trait WithDate
{
    /**
     * @var int
     */
    private $date;

    /**
     * @return int
     */
    public function getDate()
    {
        return $this->date;
    }
}
