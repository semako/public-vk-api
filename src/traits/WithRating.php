<?php

namespace semako\vkApi\traits;

use semako\vkApi\interfaces\IRating;

/**
 * Class WithRating
 * @package semako\vkApi\traits
 */
trait WithRating
{
    /**
     * @var IRating|null
     */
    private $rating;

    /**
     * @return IRating|null
     */
    public function getRating()
    {
        return $this->rating;
    }
}
