<?php

namespace semako\vkApi\traits;

use semako\vkApi\interfaces\IApplication;

/**
 * Class WithApplication
 * @package semako\vkApi\traits
 */
trait WithApplication
{
    /**
     * @var IApplication|null
     */
    private $application;

    /**
     * @return IApplication|null
     */
    public function getApplication()
    {
        return $this->application;
    }
}
