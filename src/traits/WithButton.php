<?php

namespace semako\vkApi\traits;

use semako\vkApi\interfaces\IButton;

/**
 * Class WithButton
 * @package semako\vkApi\traits
 */
trait WithButton
{
    /**
     * @var IButton|null
     */
    private $button;

    /**
     * @return IButton|null
     */
    public function getButton()
    {
        return $this->button;
    }
}
