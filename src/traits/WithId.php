<?php

namespace semako\vkApi\traits;

/**
 * Class WithId
 * @package semako\vkApi\traits
 */
trait WithId
{
    /**
     * @var int
     */
    private $id;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}
